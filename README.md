# PAFramework

[![CI Status](http://img.shields.io/travis/José Manuel/PAFramework.svg?style=flat)](https://travis-ci.org/José Manuel/PAFramework)
[![Version](https://img.shields.io/cocoapods/v/PAFramework.svg?style=flat)](http://cocoadocs.org/docsets/PAFramework)
[![License](https://img.shields.io/cocoapods/l/PAFramework.svg?style=flat)](http://cocoadocs.org/docsets/PAFramework)
[![Platform](https://img.shields.io/cocoapods/p/PAFramework.svg?style=flat)](http://cocoadocs.org/docsets/PAFramework)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PAFramework is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "PAFramework"

## Author

José Manuel, buscarini@gmail.com

## License

PAFramework is available under the MIT license. See the LICENSE file for more info.

