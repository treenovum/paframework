//
//  PALog.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef PAFW_FW_MAIN_APP_AVAILABLE
#import "DDLog.h"
#endif

#ifdef PAFW_FW_MAIN_APP_AVAILABLE

#ifdef DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif


#define PALogError(frmt, ...)	DDLogError(frmt, ##__VA_ARGS__)
#define PALogWarn(frmt, ...)	DDLogWarn(frmt, ##__VA_ARGS__)
#define PALogInfo(frmt, ...)	DDLogInfo(frmt, ##__VA_ARGS__)
#define PALogVerbose(frmt, ...)	DDLogVerbose(frmt, ##__VA_ARGS__)
#else
#define PALogError(frmt, ...)	NSLog(frmt, ##__VA_ARGS__)
#define PALogWarn(frmt, ...)	NSLog(frmt, ##__VA_ARGS__)
#define PALogInfo(frmt, ...)	NSLog(frmt, ##__VA_ARGS__)
#define PALogVerbose(frmt, ...)	NSLog(frmt, ##__VA_ARGS__)
#endif

@interface PALog : NSObject

+ (PALog *) sharedInstance;

- (void) setup;

- (NSString *) lastLogs;
- (void) clearLogs;

@end
