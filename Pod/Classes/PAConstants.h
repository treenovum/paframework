//
//  Constants.h
//  Extranet
//
//  Created by Javier Querol on 14/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

//#define USE_FAKE_WEB_SERVICES 1

#define REACHABILITY_CHANGED_NOTIFICATION @"ReachabilityChanged"
#define DATA_LOADED_NOTIFICATION @"DataLoaded"

#define USER_INFO @"user_info"

#define SHA_STRING @"sha256"
#define DOMAIN_STRING @"domains.json"

#define PUSH_NOTIFICATION_SUBAPP_ID @"subApp_id"
#define PUSH_NOTIFICATION_TOKEN @"core_app_push_notifications_token"

#define PA_PRIVATE_OPEN_BANNER_NOTIFICATION @"PA_OpenBanner"

#define CORE_APP_LIST_VIEW_MARGIN 20

// Desktop cells appearance
#define CELL_HEIGHT 44
//#define CELL_HEIGHT 55
#define CELL_HEIGHT_IPAD 60
//#define IMAGE_MARGIN 50
#define IMAGE_MARGIN_IPAD 60
#define CELL_TEXT_MARGIN 5

#define TITLE_FONT_SIZE 16
#define TITLE_FONT_SIZE_IPAD 19
#define DESCRIPTION_FONT_SIZE 12
#define DESCRIPTION_FONT_SIZE_IPAD 14
#define LABELS_OFFSET 4
#define LABELS_OFFSET_IPAD 10

#define ANIMATION_TIME 0.5

#define MAP_WIDTH_MARGIN 15 // Percentage
#define MAP_HEIGHT_MARGIN 15
#define MAP_DEFAULT_SPAN 0.005f


#define DIC_NAVIGATION_TINT_COLOR @"navigationTintColor"
#define DIC_NAVIGATION_TEXT_COLOR @"navigationTextColor"
#define DIC_BACKGROUND_COLOR @"backgroundColor"
#define DIC_ACCESSORY @"accessory"
#define DIC_NONE @"none"
#define DIC_APPS @"apps"
#define DIC_WIDGETS @"widgets"
#define DIC_DESKTOP @"desktop"
#define DIC_USER @"user"
#define DIC_PASSWORD @"password"
#define DIC_OTHER @"other"
#define DIC_COREGROUP @"COREGROUP"
#define DIC_PREFIX @"prefix"
#define DIC_CHILDREN @"children"
#define DIC_PARAMETERS @"parameters"
#define DIC_TINTCOLOR @"tintColor"
#define DIC_URL @"url"
#define DIC_NAME @"name"
#define DIC_LOGIN @"login"
#define DIC_LASTNAME @"lastName"
#define DIC_IDENTIFIER @"id"
#define DIC_PROFILES @"profiles"
#define DIC_DOMAIN @"domain"
#define DIC_DOMAINS @"domains"
#define DIC_EMAIL @"email"
#define DIC_LEVEL @"level"
#define DIC_ERROR @"error"
#define DIC_LOGIN_FAIL @"userLoginFail"
#define DIC_VERSIONCHECKING @"versionChecking"

#define NAVIGATION_ICON_SIZE 65

#define FAST_CELL_INSET 10
#define FAST_CELL_SIZE_HEIGHT 130
#define FAST_CELL_SIZE_WIDTH 90
#define FAST_CELL_NUMBER 5

#define NOTIFICATION_DELETE_ICON @"deleteIconNotification"
#define NOTIFICATION_EDITING_MODE @"editingModeEnable"

#define CELL_MASTER_HEIGHT 100
#define MAX_LOG_SIZE 500000

#define URL_RESET_PASSWORD @"http://81.18.24.170:7770/changePassword.aspx?userID=%@&newPassword=%@"

#define URL_PLIST @"http://81.18.24.170:7171/PAExtranet/Extranet.plist"

#define URL_DOMAIN @"http://81.18.24.170:7770/getDomains.aspx?userVersion=%@"

#define URL_LOGIN_MANTIS @"http://fz-mantis.postauto.ch/NCH/login.php"
#define URL_LOGOUT_MANTIS @"http://fz-mantis.postauto.ch/NCH/logout_page.php"
#define URL_DESTINATION_MANTIS @"http://fz-mantis.postauto.ch/NCH/my_view_page.php"

#define LOGIN_USERNAME @"login"
#define LOGIN_PASSWORD @"password"
#define LOGIN_DOMAIN @"domain"
#define LOGIN_SHA256 @"password"

#define LOGIN_MANTIS_USERNAME @"username"
#define LOGIN_MANTIS_PASSWORD @"password"

#define LOGIN_MANTIS_STORED_USERNAME @"storedUsernameMantis"
#define LOGIN_MANTIS_STORED_PASSWORD @"storedPasswordMantis"
#define MANTIS_LOGOUT @"login_page.php"

#define WID_OFFSET 10

#define USER_PROFILE_FOOTER_HEIGHT 130
#define USER_PROFILE_FOOTER_HEIGHT_OFFSET_UPDOWN 15
#define USER_PROFILE_FOOTER_HEIGHT_OFFSET_SIDES 15
#define USER_PROFILE_BUTTON_WIDTH 250
#define USER_PROFILE_BUTTON_HEIGHT 40
#define USER_PROFILE_WIDTH 400

#define CHECK_UPDATES YES

#define CRASHLYTICS_KEY @"cdbd2d22daba1f9d018495338f9dfeee0b18a711"

