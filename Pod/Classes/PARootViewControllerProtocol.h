//
//  PARootViewControllerProtocol.h
//  Extranet
//
//  Created by José Manuel Sánchez on 31/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PARootViewControllerProtocol <NSObject>

- (id)initWithParameters:(NSDictionary *)params nibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil;

@end
