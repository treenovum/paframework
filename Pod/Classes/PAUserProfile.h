//
//  TNUserProfiles.h
//  Extranet
//
//  Created by Javier Querol on 28/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAUserProfile : NSObject

@property (strong, nonatomic) NSString *identifier;
@property (assign, nonatomic) int level;
@property (strong, nonatomic) NSString *name;

- (id)initWithDic:(NSDictionary *)dic;

@end
