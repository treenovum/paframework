//
//  PAUtils.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAUtils.h"

#import "PALog.h"

#import <CommonCrypto/CommonDigest.h>

#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <sys/xattr.h>

@implementation PAUtils

+ (NSString *)applicationDocumentsDirectory {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
	return basePath;
}

+ (NSString *)applicationCacheDirectory {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
	return basePath;
}

+ (BOOL) isRetinaDisplay {
	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2){
		return YES;
	}
	return NO;
}

+ (NSDictionary *) parseParameters: (NSURL *) url {
	
	NSString *parameterString = [url query];
	
	if (!parameterString || [parameterString isEqualToString:@""]) return nil;
	
	parameterString = [PAUtils unescapeURLString:parameterString];
	
	NSMutableDictionary *result = [NSMutableDictionary dictionary];
	
	NSArray *components = [parameterString componentsSeparatedByString:@"&"];
	for (NSString *component in components) {
		NSArray *pair = [component componentsSeparatedByString:@"="];
		if (pair && [pair count]==2) {
			
			[result setValue:[pair objectAtIndex:1] forKey:[pair objectAtIndex:0]];
		}
		else {
//			[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing parameter: %@",component] kind:@"utils" level:core_fw_ERROR];
		}
	}
	
	return result;
}

+ (NSString *) escapeURLString: (NSString *)url {
	NSString *result = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
																		   NULL,
																		   (CFStringRef)url,
																		   NULL,
																		   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
																		   kCFStringEncodingUTF8 ));
	
	return result;
}

+ (NSString *) unescapeURLString: (NSString *)url {
	return [url stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

+ (NSString *) macaddress{
    
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    
    return outstring;
}

+ (NSString *) uniqueDeviceIdentifier{
	
//#warning REmove THISSSSSSS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//	return @"C533A70C-4922-4A7D-BE1F-62B2A4105A10";
	
	NSString *uuid = nil;

	uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
	
	PALogInfo(@"Global device identifier: %@",uuid);
	
	return uuid;
}

+ (NSString *) uniqueGlobalDeviceIdentifier {
	return [PAUtils uniqueDeviceIdentifier];
}

//+ (NSString *) uniqueGlobalDeviceIdentifier{
//    NSString *macaddress = [self macaddress];
//    NSString *uniqueIdentifier = [self CORE_stringFromMD5:macaddress];
//    
//	NSLog(@"Unique global device id: %@",uniqueIdentifier);
//	
//    return uniqueIdentifier;
//}

+ (NSString *) CORE_stringFromMD5:(NSString *)string {
    
    if(string == nil || [string length] == 0)
        return nil;
    
    const char *value = [string UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (unsigned int)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

+ (BOOL) markFileSkipBackup: (NSURL *)url {
    const char* filePath = [[url path] fileSystemRepresentation];
	
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
	
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

+ (void) showNavigationBarLoading: (UIViewController *)vc {
	UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
	[activityIndicator startAnimating];
	UIBarButtonItem * barButton = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
	
	vc.navigationItem.rightBarButtonItem = barButton;
}

+ (void) hideNavigationBarLoading: (UIViewController *)vc {
	vc.navigationItem.rightBarButtonItem = nil;
}

+ (uint64_t)getFreeDiskspace
{
//    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
	
    __autoreleasing NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
	
    if (dictionary) {
//        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        //totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
//        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        PALogError(@"Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
	
    return totalFreeSpace;
}

//+ (uint64_t)getFreeDiskspace {
//    uint64_t totalSpace = 0;
//    uint64_t totalFreeSpace = 0;
//    NSError *error = nil;
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSDictionary *dictionary = [[NSFileManager defaultManager] attributesOfFileSystemForPath:[paths lastObject] error: &error];
//	
//    if (dictionary) {
//        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
//        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
//        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
//        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
////        NSLog(@"Memory Capacity of %llu MiB with %llu MiB Free memory available.", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
//    } else {
//        NSLog(@"Error Obtaining System Memory Info: %@", error);
//    }
//	
//    return totalFreeSpace;
//}

+ (NSString *)currentLangCode {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = [[defaults objectForKey:@"AppleLanguages"] objectAtIndex:0];
    
    if (![language isEqualToString:@"en"] && ![language isEqualToString:@"it"] && ![language isEqualToString:@"de"]) language = @"en";
	
	return language;
}

@end
