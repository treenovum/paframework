//
//  PAMainApp.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAMainApp.h"
#import "PAUser.h"

static BOOL isMainAppAvailable = NO;
static NSString *currentAppId = nil;
static PAUser *currentUser = nil;
static NSArray *apps = nil;

@implementation PAMainApp

+ (void) makeMainAppAvailable {
	isMainAppAvailable = YES;
}

+ (void) makeMainAppUnavailable {
	isMainAppAvailable = NO;
}

+ (BOOL) mainAppAvailable {
	return isMainAppAvailable;
}

+ (void) setCurrentAppId: (NSString *) appId {
	if ([currentAppId isEqual:appId] || [currentAppId isEqualToString:appId]) return;
	currentAppId = appId;
}

+ (void)setCurrentUser:(PAUser *)user {
	if (currentUser==user) return;
	currentUser = user;
}

+ (PAUser *)currentUser {
	return currentUser;
}

+ (NSString *) currentAppId {
	return currentAppId;
}

+ (NSArray *) allApps {
	return apps;
}

+ (void) setAllApps:(NSArray *) appsArray; {
	apps = appsArray;
}

@end
