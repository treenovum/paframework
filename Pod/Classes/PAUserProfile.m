//
//  TNUserProfiles.m
//  Extranet
//
//  Created by Javier Querol on 28/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAUserProfile.h"

#define DIC_IDENTIFIER @"id"
#define DIC_LEVEL @"level"
#define DIC_NAME @"name"

@implementation PAUserProfile

- (id)initWithDic:(NSDictionary *)dic {
	self = [super init];
	if (!self) return nil;
	
	self.identifier = [dic valueForKey:DIC_IDENTIFIER];
	self.level = [[dic valueForKey:DIC_LEVEL] intValue];
	self.name = [dic valueForKey:DIC_NAME];
	
	return self;
}

@end
