//
//  PAFavorite.m
//  Extranet
//
//  Created by José Manuel Sánchez on 31/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAFavorite.h"

#import "PAFramework.h"

@implementation PAFavorite

- (NSDictionary *) serialize {
	
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	if (self.title) dict[@"title"] = self.title;
	if (self.itemId) dict[@"ident"] = self.itemId;
	if (self.appId) dict[@"appId"] = self.appId;
	if (self.parameters) dict[@"parameters"] = self.parameters;
	
	return dict;
}


- (void) load: (NSDictionary *) dict {
	self.title = dict[@"title"];
	self.itemId = dict[@"ident"];
	self.appId = dict[@"appId"];
	self.parameters = dict[@"parameters"];
}

@end
