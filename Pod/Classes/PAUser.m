//
//  TNUser.m
//  Extranet
//
//  Created by Javier Querol on 28/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAUser.h"
#import "PAConstants.h"
#import "PAUserProfile.h"

#import "PADataParser.h"

@implementation PAUser

- (id)initWithDic:(NSDictionary *)dic {

	if (!dic || (NSNull *)dic==[NSNull null]) return nil;
	
	self = [super init];
	if (!self) return nil;
	
	self.dictionary = dic;
	
	self.name = [PADataParser parseStringValue:dic key:DIC_NAME optional:YES];
	self.lastName = [PADataParser parseStringValue:dic key:DIC_LASTNAME optional:YES]; //[dic valueForKey:DIC_LASTNAME];
	self.login = [PADataParser parseStringValue:dic key:DIC_LOGIN optional:YES];//[dic valueForKey:DIC_LOGIN];
	self.identifier = [[PADataParser parseNumberValue:dic key:DIC_IDENTIFIER optional:YES] stringValue];//[dic valueForKey:DIC_IDENTIFIER];
	self.email = [PADataParser parseStringValue:dic key:DIC_EMAIL optional:YES];//[dic valueForKey:DIC_EMAIL];
	
	NSArray *array = [dic valueForKey:DIC_PROFILES];
	self.profiles = [NSMutableArray array];
	
	for (NSDictionary *dic in array) {
		PAUserProfile *profile = [[PAUserProfile alloc] initWithDic:dic];
		[self.profiles addObject:profile];
	}
	
	return self;
}

@end
