//
//  URLLoader.h
//
//  Copyright (c) 2012 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^StringCompletionBlock)(NSString *result);
typedef void(^ImageCompletionBlock)(UIImage *image);
typedef void(^DataCompletionBlock)(NSData *data);

@interface PAURLLoader : NSObject <NSURLConnectionDelegate> {
	NSMutableData *receivedData;
	StringCompletionBlock completionBlock;
    ImageCompletionBlock imageCompletionBlock;
	DataCompletionBlock dataCompletionBlock;

    BOOL isImage;
	BOOL isString;
	
	NSURLConnection *currentConnection;
}

@property (assign, nonatomic) NSInteger timeout;

@property (assign, nonatomic) NSString *user;
@property (assign, nonatomic) NSString *password;


- (BOOL) loading;

- (void) loadDataFromURL: (NSString *)url completion:(DataCompletionBlock)block;
- (void) loadDataFromURL: (NSString *)url params: (NSString *)params usePOST:(BOOL)usePOSTMethod completion:(DataCompletionBlock)block;

- (void) loadStringFromURL: (NSString *)url completion:(StringCompletionBlock)block;
- (void) loadStringFromURL: (NSString *)url params: (NSString *)params usePOST:(BOOL)usePOSTMethod completion:(StringCompletionBlock)block;

- (void) loadImageFromURL: (NSString *)url completion:(ImageCompletionBlock)block;
- (void) loadImageFromURL: (NSString *)url params: (NSString *)params usePOST:(BOOL)usePOSTMethod completion:(ImageCompletionBlock)block;


- (NSMutableURLRequest *) postRequestWithUrl:(NSString *)urlString params: (NSString *)params;
- (NSMutableURLRequest *) getRequestWithUrl:(NSString *)urlString params: (NSString *)params;

+ (NSString *) urlEncodedString: (NSString *)string;

// Extra params to give info to the server about the current user, lang, etc
+ (NSDictionary *) infoHeaderParams;

@end
