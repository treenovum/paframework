// Code from: http://www.mlsite.net/blog/?p=232
// Modified to include a shadow

#import "UIButton+PAGlossy.h"
#import <QuartzCore/QuartzCore.h>

void UIButton_PAGlossy_touch(void);

void UIButton_PAGlossy_touch()
{
    // This is a trick to help categories link properly in static libraries
    NSLog(@"Do nothing, just to make categories link correctly for static files.");
}

@implementation UIButton (PAGlossy)

- (void)makeGlossy {
    CALayer *thisLayer = self.layer;
	
	
	
    // Add a border
    thisLayer.cornerRadius = 8.0f;
    thisLayer.masksToBounds = NO;
    thisLayer.borderWidth = 1.5f;
    thisLayer.borderColor = self.backgroundColor.CGColor;
	
    // Give it a shadow
    if ([thisLayer respondsToSelector:@selector(shadowOpacity)]) // For compatibility, check if shadow is supported
    {
        thisLayer.shadowOpacity = 1;
        thisLayer.shadowColor = [[UIColor blackColor] CGColor];
        thisLayer.shadowOffset = CGSizeMake(0.0, 1.0);
		thisLayer.shadowRadius = 2;
        
        // TODO: Need to test these on iPad
        if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2)
        {
            thisLayer.rasterizationScale=2.0;
        }
		thisLayer.shouldRasterize = YES; // FYI: Shadows have a poor effect on performance
    }
	
	CALayer *backgroundLayer = nil;
	CAGradientLayer *glossLayer = nil;
	for (CALayer *layer in thisLayer.sublayers) {
		if ([layer.name isEqualToString:@"PAGlossyBackgroundLayer"]) {
			backgroundLayer = layer;
			break;
		}
		if ([layer.name isEqualToString:@"PAGlossLayer"]) {
			glossLayer = (CAGradientLayer *)layer;
			break;
		}
	}
	
    // Add backgorund color layer and make original background clear
    if (!backgroundLayer) {
		backgroundLayer = [CALayer layer];
		
		backgroundLayer.name = @"PAGlossyBackgroundLayer";
		backgroundLayer.cornerRadius = 8.0f;
		backgroundLayer.masksToBounds = YES;
		backgroundLayer.backgroundColor = self.backgroundColor.CGColor;
		[thisLayer insertSublayer:backgroundLayer atIndex:0];
		
		thisLayer.backgroundColor=[UIColor colorWithWhite:0.0f alpha:0.0f].CGColor;
		
		// Add gloss to the background layer
		if (!glossLayer) {
			glossLayer = [CAGradientLayer layer];
			glossLayer.name = @"PAGlossLayer";
			
			glossLayer.colors = [NSArray arrayWithObjects:
								 (id)[UIColor colorWithWhite:1.0f alpha:0.4f].CGColor,
								 (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
								 (id)[UIColor colorWithWhite:0.75f alpha:0.0f].CGColor,
								 (id)[UIColor colorWithWhite:1.0f alpha:0.2f].CGColor,
								 nil];
			glossLayer.locations = [NSArray arrayWithObjects:
									[NSNumber numberWithFloat:0.0f],
									[NSNumber numberWithFloat:0.5f],
									[NSNumber numberWithFloat:0.5f],
									[NSNumber numberWithFloat:1.0f],
									nil];
			[backgroundLayer addSublayer:glossLayer];
		}
	}
	
	backgroundLayer.frame = thisLayer.bounds;
	glossLayer.frame = thisLayer.bounds;
}

@end

