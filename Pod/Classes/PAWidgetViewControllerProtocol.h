//
//  PAWidgetViewControllerProtocol.h
//  Extranet
//
//  Created by José Manuel Sánchez on 14/06/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PAWidgetViewControllerProtocol <NSObject>

@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSDictionary *parameters;

- (NSArray *) widgetInfos;

@end
