//
//  TNAppearance.m
//  Extranet
//
//  Created by José Manuel Sánchez on 05/06/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAAppearance.h"

static PAAppearance *instance = nil;

@implementation PAAppearance

+ (PAAppearance *) sharedInstance {
	if (!instance) {
		instance = [[PAAppearance alloc] init];
	}
	
	return instance;
}

@end
