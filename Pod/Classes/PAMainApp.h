//
//  PAMainApp.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PAUser.h"

@interface PAMainApp : NSObject

+ (void) makeMainAppAvailable;
+ (void) makeMainAppUnavailable;
+ (BOOL) mainAppAvailable;

+ (void) setCurrentAppId: (NSString *) appId;
+ (NSString *) currentAppId;

+ (void)setCurrentUser:(PAUser *)user;
+ (PAUser *)currentUser;

+ (NSArray *) allApps;
+ (void) setAllApps:(NSArray *) apps;

@end
