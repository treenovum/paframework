//
//  PALog.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PALog.h"

#import "DDASLLogger.h"
#import "DDTTYLogger.h"
#import "DDFileLogger.h"


static PALog *instance = nil;

@interface PALog() {
	DDFileLogger *fileLogger;
}

@end

@implementation PALog

+ (PALog *) sharedInstance {
	if (!instance) {
		instance = [[PALog alloc] init];
	}
	
	return instance;
}

- (void) setup {
	[DDLog addLogger:[DDASLLogger sharedInstance]];
	[DDLog addLogger:[DDTTYLogger sharedInstance]];
	
	fileLogger = [[DDFileLogger alloc] init];
	fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
	fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
	
	[DDLog addLogger:fileLogger];
}

- (NSString *) lastLogs {
	
	id<DDLogFileManager> logManager = fileLogger.logFileManager;
	
	if (logManager.sortedLogFilePaths.count==0) return @"";
	
	NSString *path = logManager.sortedLogFilePaths[logManager.sortedLogFilePaths.count-1];
	
	NSError *error = nil;
	
	return [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
}

- (void) clearLogs {
	id<DDLogFileManager> fileManager = fileLogger.logFileManager;
	
	NSArray *paths = [fileManager sortedLogFilePaths];
	
	for (NSString *path in paths) {
		NSError *error = nil;
		[[NSFileManager defaultManager] removeItemAtPath:path error:&error];
	}
	
}

@end
