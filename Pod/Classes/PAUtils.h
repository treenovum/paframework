//
//  PAUtils.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

@interface PAUtils : NSObject

+ (NSString *)applicationDocumentsDirectory;
+ (NSString *)applicationCacheDirectory;
+ (BOOL) isRetinaDisplay;

+ (NSDictionary *) parseParameters: (NSURL *) url;

+ (NSString *) escapeURLString: (NSString *)urlParameter;
+ (NSString *) unescapeURLString: (NSString *)urlParameter;

+ (NSString *) uniqueDeviceIdentifier;
+ (NSString *) uniqueGlobalDeviceIdentifier __deprecated;

+ (BOOL) markFileSkipBackup: (NSURL *)url;

+ (void) showNavigationBarLoading: (UIViewController *)vc;
+ (void) hideNavigationBarLoading: (UIViewController *)vc;

+ (uint64_t)getFreeDiskspace;

+ (NSString *)currentLangCode;
	
@end
