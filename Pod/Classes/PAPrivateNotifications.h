//
//  PAPrivateNotifications.h
//  Extranet
//
//  Created by José Manuel Sánchez on 06/08/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void(^NotificationsSettingsBlock)(NSMutableDictionary *settings);
typedef void(^NotificationsBreakIntervalBlock)(NSMutableDictionary *breakSettings);
typedef void(^NotificationsSaveBlock)(BOOL result);
typedef void(^NotificationsLoadWholeBlock)(NSDictionary *message);

@interface PAPrivateNotifications : NSObject <NSURLConnectionDelegate> {
	NSMutableData *data;
	
	NSMutableData *tagsData;
	NSMutableData *saveTagsData;
	
	NSMutableData *breakIntervalData;
	NSMutableData *saveBreakIntervalData;
	
	NSURLConnection *tagSettingsConnection;
	NSURLConnection *saveTagSettingsConnection;
	NSURLConnection *breakIntervalConnection;
	NSURLConnection *saveBreakIntervalConnection;
	
	NotificationsSettingsBlock tagSettingsBlock;
	NotificationsSaveBlock saveTagBlock;
	
	NotificationsBreakIntervalBlock breakIntervalBlock;
	NotificationsSaveBlock saveBreakIntervalBlock;
}

+ (PAPrivateNotifications *)Get;

+ (void) registerDeviceToken: (NSString *) token;

+ (NSString *)getNotificationId;

- (BOOL) tagSettings: (NSString *) appId completion:(NotificationsSettingsBlock) block;
- (BOOL) saveTagSettings: (NSDictionary *)settings forApp: (NSString *) appId completion:(NotificationsSaveBlock) block;

- (BOOL) breakIntervalSettings: (NSString *)appId completion:(NotificationsBreakIntervalBlock) block;
- (BOOL) saveBreakIntervalSettings: (NSDictionary *) settings forApp: (NSString *) appId completion:(NotificationsSaveBlock) block;


- (void) loadWholeMessage: (NSDictionary *)userInfo completion: (NotificationsLoadWholeBlock) block;

@end
