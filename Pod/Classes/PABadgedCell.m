
#import "PABadgedCell.h"

#import <QuartzCore/QuartzCore.h>

#define BADGE_RIGHT_MARGIN 5

@implementation PABadgedCell

@synthesize badgeString, badgeColor, badgeColorHighlighted, loading;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
		self.badgeColor = [UIColor colorWithRed:0.748 green:0.000 blue:0.000 alpha:1.000];
		self.badgeColorHighlighted = [UIColor whiteColor];
    }
    return self;
}

- (void) dealloc {
	[loaderView removeFromSuperview];
	[label removeFromSuperview];
//	[loaderView release];
//	[label release];
//	[super dealloc];
}

- (void) setBadgeString:(NSString *)string {
	
	badgeString = string;
	
	if (!badgeString || [badgeString isEqualToString:@""] || [badgeString isEqualToString:@"0"]) {
		self.accessoryView = nil;
		return;
	}
	
	CGSize size = [badgeString boundingRectWithSize:CGSizeMake(50, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: [UIFont boldSystemFontOfSize:14] } context:nil].size;
	
	size.width += 20;
	size.height += 6;
	
//	if (label) [label release];
	[label removeFromSuperview];
	label = [[UILabel alloc] initWithFrame:CGRectMake(self.contentView.frame.size.width-size.width-BADGE_RIGHT_MARGIN, (self.contentView.frame.size.height-size.height)/2, size.width, size.height)];
	label.textAlignment = NSTextAlignmentCenter;
	label.font = [UIFont boldSystemFontOfSize:14];
	label.layer.cornerRadius = size.height/2;
	label.layer.masksToBounds = YES;
	label.textColor = [UIColor whiteColor];
	label.backgroundColor = self.badgeColor;
	label.highlightedTextColor = self.badgeColorHighlighted;
	label.text = badgeString;
	label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

	[loaderView removeFromSuperview];
	[self.contentView addSubview:label];
	
	//self.accessoryView = label;
}

- (void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
	[super setHighlighted:highlighted animated:animated];
	
	[self updateBadge];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
	
	[self updateBadge];
}

- (void) setLoading:(BOOL)newLoading {
	loading = newLoading;
	
	if (loading) {
		[label removeFromSuperview];
		label = nil;
		
		loaderView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[loaderView startAnimating];
		loaderView.frame = CGRectMake(self.contentView.frame.size.width-loaderView.frame.size.width-BADGE_RIGHT_MARGIN, (self.contentView.frame.size.height-loaderView.frame.size.height)/2, loaderView.frame.size.width, loaderView.frame.size.height);
		loaderView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
		//self.accessoryView = loaderView;
		[self.contentView addSubview:loaderView];
	}
	else {
		[loaderView removeFromSuperview];
		loaderView = nil;
	}
}

+ (CGFloat) badgeWidth: (NSString *) badgeString {

	CGSize size = [badgeString boundingRectWithSize:CGSizeMake(50, 1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: [UIFont boldSystemFontOfSize:14] } context:nil].size;
	
	size.width += 20;

	return size.width;
}

+ (CGFloat) loaderWidth {
	UIActivityIndicatorView *loaderView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	return loaderView.frame.size.width;
}

- (void) updateBadge {
	
	//UILabel *label = (UILabel *)self.accessoryView;
	//if ([label isKindOfClass:[UIActivityIndicatorView class]]) return;
	
    if (self.selected || self.highlighted) {
		if (label) {
			label.backgroundColor = self.badgeColorHighlighted;
			label.textColor = self.badgeColor;
			label.highlightedTextColor = self.badgeColor;
		}
		
    }
	else {
		if (label) {
			label.backgroundColor = self.badgeColor;
			label.textColor = self.badgeColorHighlighted;
			label.highlightedTextColor = self.badgeColorHighlighted;
		}
	}
}

- (void) layoutSubviews {
	[super layoutSubviews];

	CGRect titleFrame = self.textLabel.frame;
	
	if (label && label.superview==self.contentView) {
		titleFrame.size.width = label.frame.origin.x-titleFrame.origin.x;
	}
	else if (loaderView && loaderView.superview==self.contentView) {
		titleFrame.size.width = loaderView.frame.origin.x-titleFrame.origin.x;
	}
	else {
		titleFrame.size.width = self.contentView.frame.size.width-titleFrame.origin.x;
	}
	
	self.textLabel.frame = titleFrame;
}

@end