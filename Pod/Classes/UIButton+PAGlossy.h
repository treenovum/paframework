#import <UIKit/UIKit.h>

void UIButton_PAGlossy_touch();

@interface UIButton (PAGlossy)

- (void) makeGlossy;

@end
