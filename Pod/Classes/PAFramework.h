//
//  PAFramework.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PAConstants.h"
#import "PADefinitions.h"
#import "PAUtils.h"
#import "PALog.h"
#import "PAApplicationDelegate.h"
#import "PARootViewControllerProtocol.h"
#import "PAFavorite.h"
#import "PAAppearance.h"
#import "PAWidgetViewControllerProtocol.h"
#import "PAUser.h"
#import "PAUserProfile.h"
#import "PAURLLoader.h"
#import "UIButton+PAGlossy.h"

@protocol PAProtocol <NSObject>
@property (strong) NSDictionary *params;
@end

@interface PAFramework : NSObject

+ (void) enableDebugMode;
+ (void) disableDebugMode;
+ (BOOL) inDebugMode;

+ (float) version;

+ (BOOL) mainAppAvailable;
+ (void) showMainApp;
+ (void) openApp: (NSString *) appId parameters:(NSDictionary *)parameters;

+ (BOOL) isInstalled:(NSString *)appId;
+ (BOOL) isAccesible:(NSString *)appId;

+ (void) init;
+ (void) finish;

+ (NSString *) currentAppId;

+ (void) addFavorite: (PAFavorite *) favorite;
+ (void) removeFavorite: (NSString *) ident appId:(NSString *) appId;

+ (BOOL) isFavorite:(NSString *) ident appId:(NSString *) appId;

+ (NSArray *) favorites;
+ (PAUser *)currentUser;

+ (NSString *)langCode;

+ (void) loadFile:(NSURL *)url completion:(PADataCompletionBlock)block;

+ (void) loadImage:(NSURL *)url completion:(PAImageCompletionBlock)block;
+ (void)loadImageView:(UIImageView *)imageView withURL:(NSURL *)url andPlaceholder:(UIImage *)placeholder completion:(PACompletionBlock) block;

@end
