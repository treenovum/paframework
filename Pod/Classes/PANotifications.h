//
//  PANotifications.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^SettingsBlock)(NSMutableDictionary *settings);
typedef void(^BreakIntervalBlock)(NSMutableDictionary *breakSettings);
typedef void(^SaveBlock)(BOOL result);

@interface PANotifications : NSObject

+ (NSString *) serverDeviceId;

+ (BOOL) tagSettings: (NSString *) appId completion: (SettingsBlock) block;
+ (BOOL) saveTagSettings: (NSDictionary *)settings forApp: (NSString *) appId completion: (SaveBlock) block;

+ (BOOL) breakIntervalSettings: (NSString *)appId completion: (BreakIntervalBlock) block;
+ (BOOL) saveBreakIntervalSettings: (NSDictionary *) settings forApp: (NSString *) appId completion:(SaveBlock) block;

@end
