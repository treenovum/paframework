//
//  PAFavorite.h
//  Extranet
//
//  Created by José Manuel Sánchez on 31/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAFavorite : NSObject

@property (strong, nonatomic) NSString *title;

@property (strong, nonatomic) NSString *itemId;
@property (strong, nonatomic) NSString *appId;
@property (strong, nonatomic) NSDictionary *parameters;

- (NSDictionary *) serialize;
- (void) load: (NSDictionary *) dict;

@end
