//
//  TNDataParser.h
//  cedam
//
//  Created by Jose Manuel Sánchez Peñarroja on 19/06/12.
//  Copyright (c) 2012 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PADataParser : NSObject

+ (NSMutableArray *) parseArrayValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (NSMutableDictionary *) parseDictionaryValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (NSString *) parseStringValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (NSNumber *) parseNumberValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (NSInteger) parseIntegerValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (CGFloat) parseFloatValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (NSDate *) parseDateValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;
+ (UIColor *) parseColorValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional;

+ (BOOL) checkIsArray: (id) object key:(NSString *)key optional:(BOOL)optional;
+ (BOOL) checkIsDictionary: (id) object key:(NSString *)key optional:(BOOL)optional;
+ (BOOL) checkIsString: (id) object key:(NSString *)key optional:(BOOL)optional;
+ (BOOL) checkIsNumber: (id) object key:(NSString *)key optional:(BOOL)optional;
+ (BOOL) checkIsDate: (id) object key:(NSString *)key optional:(BOOL)optional;
+ (BOOL) checkObjectClass: (id) object class: (Class) c key:(NSString *)key optional:(BOOL) optional;

@end
