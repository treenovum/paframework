//
//  TNAppearance.h
//  Extranet
//
//  Created by José Manuel Sánchez on 05/06/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAAppearance : NSObject

+ (PAAppearance *) sharedInstance;

@property (strong, nonatomic) UIColor *navigationBarTintColor;
@property (strong, nonatomic) UIColor *navigationBarBarTintColor;

@end
