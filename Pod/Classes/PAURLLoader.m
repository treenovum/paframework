//
//  URLLoader.m
//
//  Copyright (c) 2012 treeNovum. All rights reserved.
//

#import "PAURLLoader.h"

#import "PAFramework.h"

@implementation PAURLLoader


- (id)init {
    self = [super init];
    if (self) {
        self.timeout = 30;
    }
    return self;
}

- (BOOL) loading {
	if (currentConnection) return YES;
	return NO;
		
}

- (NSMutableURLRequest *) postRequestWithUrl:(NSString *)urlString params: (NSString *)params {

	NSData *postData = nil;
	NSString *postLength = nil;
	if (params) {
		postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
	}
	
	NSURL *url = [NSURL URLWithString:urlString];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:self.timeout];
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	if (params) {
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:postData];
	}
	
	return request;
}

- (NSMutableURLRequest *) getRequestWithUrl:(NSString *)urlString params: (NSString *)params {

	NSString *finalUrlString = urlString;
	if (params) finalUrlString = [NSString stringWithFormat:@"%@?%@",urlString,params];
	NSURL *url = [NSURL URLWithString:finalUrlString];
	return [NSMutableURLRequest requestWithURL:url];
}

- (void) loadDataFromURL: (NSString *)url completion:(DataCompletionBlock)block {
	[self loadDataFromURL:url params:nil usePOST:NO completion:block];
}

- (void) loadStringFromURL: (NSString *)urlString completion:(StringCompletionBlock)block {
	[self loadStringFromURL:urlString params:nil usePOST:NO completion:block];
}

- (void) loadImageFromURL: (NSString *)urlString completion:(ImageCompletionBlock)block {
	[self loadImageFromURL:urlString params:nil usePOST:NO completion:block];
}

// [request addRequestHeader:@"Authorization" value:[NSString stringWithFormat:@"Basic %@",[ASIHTTPRequest base64forData:[[NSString stringWithFormat:@"%@:%@",theUsername,thePassword] dataUsingEncoding:NSUTF8StringEncoding]]]];
- (void)loadDataFromURL:(NSString *)url params:(NSString *)params usePOST:(BOOL)usePOSTMethod completion:(DataCompletionBlock)block {
	dataCompletionBlock = [block copy];
	receivedData = [NSMutableData data];
	isImage = NO;
	isString = NO;
	
	NSMutableURLRequest *request = nil;
	if (usePOSTMethod) {
		request = [self postRequestWithUrl:url params:params];
	}
	else {
		request = [self getRequestWithUrl:url params:params];
	}
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	/*[request setValue:NSLocalizedString(@"de", nil) forHTTPHeaderField:@"lang"];
	PAUser *user = [PAFramework currentUser];
	if (user) {
		[request setValue:user.identifier forHTTPHeaderField:@"userid"];
	}*/
	
	
	//	NSURL *url = [NSURL URLWithString:urlString];
	//	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	if (currentConnection) [currentConnection cancel];
	currentConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	[currentConnection start];
}

- (void) loadStringFromURL: (NSString *)url params: (NSString *)params usePOST:(BOOL)usePOSTMethod completion:(StringCompletionBlock)block {
	completionBlock = [block copy];
	receivedData = [NSMutableData data];
	isImage = NO;
	isString = YES;
	
	NSURLRequest *request = nil;
	if (usePOSTMethod) {
		request = [self postRequestWithUrl:url params:params];
	}
	else {
		request = [self getRequestWithUrl:url params:params];
	}
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forKey:key];
	}];
	
	//	NSURL *url = [NSURL URLWithString:urlString];
	//	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	if (currentConnection) [currentConnection cancel];
	currentConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];

	[currentConnection start];
}

- (void) loadImageFromURL: (NSString *)url params: (NSString *)params usePOST:(BOOL)usePOSTMethod completion:(ImageCompletionBlock)block {
	imageCompletionBlock = [block copy];
	receivedData = [NSMutableData data];
    isImage = YES;
	isString = NO;
	
	NSURLRequest *request = nil;
	if (usePOSTMethod) {
		request = [self postRequestWithUrl:url params:params];
	}
	else {
		request = [self getRequestWithUrl:url params:params];
	}
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forKey:key];
	}];
	
	//	NSURL *url = [NSURL URLWithString:urlString];
	//	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	if (currentConnection) [currentConnection cancel];
	currentConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	
	[currentConnection start];
}


/*
NSData *postData = nil;
NSString *postLength = nil;
if (params) {
	postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	postLength = [NSString stringWithFormat:@"%d", [postData length]];
}

NSURL *url = [NSURL URLWithString:urlString];

[treenovum_gigathlon_ErrorManager Log:[NSString stringWithFormat:@"post request with url: %@",urlString]];

NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
[request setHTTPMethod:@"POST"];
[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
[request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
[request setValue:UDID forHTTPHeaderField:@"X-client-id"];
[request setValue:rtid forHTTPHeaderField:@"X-map-id"];
[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];

if (params) {
	[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	[request setHTTPBody:postData];
}
*/

#pragma mark -
#pragma mark NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	[receivedData setLength:0];
}


- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    //return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    //if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
      //  [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    if (self.user && self.password) {
        [challenge.sender useCredential:[NSURLCredential credentialWithUser:self.user password:self.password persistence:NSURLCredentialPersistenceForSession] forAuthenticationChallenge:challenge];
    }
    else {
        [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)[cachedResponse response];
	
    // Look up the cache policy used in our request
    if([connection currentRequest].cachePolicy == NSURLRequestUseProtocolCachePolicy) {
        NSDictionary *headers = [httpResponse allHeaderFields];
        NSString *cacheControl = [headers valueForKey:@"Cache-Control"];
        NSString *expires = [headers valueForKey:@"Expires"];
        if((cacheControl == nil) && (expires == nil)) {
            //NSLog(@"server does not provide expiration information and we are using NSURLRequestUseProtocolCachePolicy");
            return nil; // don't cache this
        }
    }
    return cachedResponse;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[receivedData appendData:data]; 
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	currentConnection = nil;
    if (isImage) {
        UIImage *image = [[UIImage alloc] initWithData:receivedData];
        imageCompletionBlock(image);
    }
    else if (isString) {
        NSString *result = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
        completionBlock(result);
    }
	else {
		dataCompletionBlock(receivedData);
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	currentConnection = nil;
	
	NSLog(@"Error loading data: %@",[error localizedDescription]);
	
    if (isImage) {
        imageCompletionBlock(nil);
    }
    else if (isString) {
        completionBlock(nil);
    }
	else dataCompletionBlock(nil);
}

+ (NSString *) urlEncodedString: (NSString *)string {
	NSString *result = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	return result;
}

+ (NSDictionary *) infoHeaderParams {
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	
	dict[@"lang"] = [PAFramework langCode]; // NSLocalizedString(@"de", nil);
	
	PAUser *user = [PAFramework currentUser];
	if (user) {
		dict[@"userid"] = user.identifier;
	}
	
	return dict;
}

@end
