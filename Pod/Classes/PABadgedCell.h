
#import <UIKit/UIKit.h>

@interface PABadgedCell : UITableViewCell {
	UILabel *label;
	UIActivityIndicatorView *loaderView;
}

@property (nonatomic, retain)   NSString *badgeString;
@property (nonatomic, retain)   UIColor *badgeColor;
@property (nonatomic, retain)   UIColor *badgeColorHighlighted;
@property (nonatomic, assign)	BOOL loading;

+ (CGFloat) badgeWidth: (NSString *) badgeString;
+ (CGFloat) loaderWidth;

@end
