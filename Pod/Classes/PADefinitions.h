//
//  PADefinitions.h
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#define PAFW_MAINAPP_TITLE NSLocalizedString(@"Home", nil)

// NOTIFICATIONS

#define PAFW_APP_PUSH_NOTIFICATION_TOKEN @"core_app_PushNotificationToken"

#define PAFW_OPEN_APP_NOTIFICATION @"PAFW_app_OpenApp"
#define PAFW_OPENED_APP_NOTIFICATION @"PAFW_app_OpenedApp"
#define PAFW_SHOW_MAINAPP_NOTIFICATION @"PAFW_app_ShowMainApp"
#define PAFW_APP_DID_ENTER_BACKGROUND @"PAFW_app_DidEnterBackGround"
#define PAFW_APP_WILL_ENTER_FOREGROUND @"PAFW_app_WillEnterForeGround"
#define PAFW_APP_RELOAD_AR @"PAFW_app_Reload_AR"
#define PAFW_PRIVATE_OPEN_URL_NOTIFICATION @"PAFW_open_url"

#define PAFW_MAIN_CONFIG_LOADED @"PAFW_app_main_config_loaded"
#define PAFW_MAIN_CONFIG_FAILED_LOADING @"PAFW_app_main_config_failed loading"
#define PAFW_RESOURCES_LOADED @"PAFW_app_resources_loaded"
#define PAFW_MAIN_BANNER_IMAGES_LOADED @"BannerImagesLoaded"

#define PAFW_RECEIVED_PUSH_NOTIFICATION @"core_app_ReceivedPushNotification"

#define NSAppLocalizedString(string,appId) [[NSBundle mainBundle] localizedStringForKey:string value:string table:appId "_Localizable"]

#define CONFIG_URL @"http://appadm.pa-app.ch/NavigationProvider/main.aspx?lang=%@&deviceId=%@&os=%@"
//#define CONFIG_URL @"http://appadm.postauto.ch/AppAdm/MainLiveServer/main_%@.xml"

//#define CONFIG_URL @"http://192.168.12.100:3333/Api/Main?os=2&lang=%@&deviceId=%@"

//#define CONFIG_URL @"http://appadm.postauto.ch/AppAdm/test_iphone/main_%@.xml"

//#define CONFIG_URL @"http://appadm.postauto.ch/AppAdm/main_en_test.aspx"
//#define CONFIG_URL @"http://81.18.24.170:7171/main_%@.xml"

//#define CONFIG_URL @"http://192.168.12.106:8080/postauto/main.xml" // Novum

//#define CPNS_URL(service) @"http://190.248.135.194:8085/CPNS/" service
//#define CPNS_URL(service) @"http://appadm.postauto.ch/cpns/" service
#define CPNS_URL(service) @"http://appadm.pa-app.ch/cpns/" service

//#define PAFW_SPLASH_TEXT_URL @"https://dl.dropboxusercontent.com/u/72768/public/postauto_message.json?lang="
//#define PAFW_SPLASH_TEXT_URL @"https://dl.dropboxusercontent.com/u/72768/public/postauto_message.json?lang="
#define PAFW_SPLASH_TEXT_URL @"https://appadm.pa-app.ch/NavigationWS/Api/SplashText?lang="

#define REGISTER_PUSH_URL CPNS_URL("Api/register")
#define UNREGISTER_PUSH_URL CPNS_URL("Api/unregister")
#define TAGS_PUSH_URL CPNS_URL("Api/tagSettings")
#define BREAKTIME_PUSH_URL CPNS_URL("Api/breakTime")
#define MESSAGE_PUSH_URL CPNS_URL("Api/getMessage")

#define CONNECTION_TIMEOUT 5


typedef void(^PACompletionBlock)(NSError *error);
typedef void(^PAImageCompletionBlock)(UIImage *image,NSError *error);
typedef void(^PADataCompletionBlock)(NSData *data,NSError *error);
typedef void(^PAResultCompletionBlock)(id result,NSError *error);

#define PASuppressPerformSelectorLeakWarning(Stuff) \
do { \
_Pragma("clang diagnostic push") \
_Pragma("clang diagnostic ignored \"-Warc-performSelector-leaks\"") \
Stuff; \
_Pragma("clang diagnostic pop") \
} while (0)
