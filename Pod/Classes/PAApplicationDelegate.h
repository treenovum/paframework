//
//  TNPAApplicationDelegate.h
//  Extranet
//
//  Created by José Manuel Sánchez on 30/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^PABadgeCompletionBlock)(NSString *badgeString);

@protocol PAApplicationDelegate <UIApplicationDelegate>

@optional
- (void) badgeString: (PABadgeCompletionBlock) block;

@end
