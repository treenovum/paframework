//
//  PANotifications.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PANotifications.h"

#import "PAPrivateNotifications.h"

@implementation PANotifications

+ (NSString *) serverDeviceId {
    return [PAPrivateNotifications getNotificationId];
}

+ (BOOL) tagSettings: (NSString *) appId completion: (SettingsBlock) block {
	return [[PAPrivateNotifications Get] tagSettings:appId completion:block];
}

+ (BOOL) saveTagSettings: (NSDictionary *)settings forApp: (NSString *) appId completion: (SaveBlock) block {
	return [[PAPrivateNotifications Get] saveTagSettings:settings forApp:appId completion:block];
}

+ (BOOL) breakIntervalSettings: (NSString *)appId completion: (BreakIntervalBlock) block {
	return [[PAPrivateNotifications Get] breakIntervalSettings:appId completion:block];
}


+ (BOOL) saveBreakIntervalSettings: (NSDictionary *) settings forApp: (NSString *) appId completion:(SaveBlock) block {
	return [[PAPrivateNotifications Get] saveBreakIntervalSettings:settings forApp:appId completion:block];
}

@end
