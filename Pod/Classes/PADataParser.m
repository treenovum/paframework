//
//  TNDataParser.m
//  cedam
//
//  Created by Jose Manuel Sánchez Peñarroja on 19/06/12.
//  Copyright (c) 2012 treeNovum. All rights reserved.
//

#import "PADataParser.h"
#import "PALog.h"

#import "UIColor+PAHex.h"

@implementation PADataParser


+ (NSString *) parseStringValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSString *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSString class]]) {
		return value;
	}
	else {
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not string",key]);
	}

	return nil;
}


+ (NSMutableArray *) parseArrayValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSMutableArray *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSMutableArray class]]) {
		return value;
	}
	else {
		
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not array",key]);
	}
	
	return nil;
}

+ (NSMutableDictionary *) parseDictionaryValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSMutableDictionary *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSMutableDictionary class]]) {
		return value;
	}
	else {
		
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not a dictionary",key]);
	}
	
	return nil;
}

+ (NSNumber *) parseNumberValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSNumber *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSNumber class]]) {
		return value;
	}
	else {
		
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not a number",key]);
	}
	
	return nil;
}

+ (NSInteger) parseIntegerValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSNumber *number = [PADataParser parseNumberValue:dict key:key optional:optional];
	if (!number) return -1;
	return [number integerValue];
}

+ (CGFloat) parseFloatValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSNumber *number = [PADataParser parseNumberValue:dict key:key optional:optional];
	if (!number) return -1;
	return [number floatValue];
}


+ (NSDate *) parseDateValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSDate *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSDate class]]) {
		return value;
	}
	else {
		
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not a date",key]);
	}
	
	return nil;
}

+ (UIColor *) parseColorValue:(NSDictionary *)dict key:(NSString *)key optional:(BOOL)optional {
	NSString *value = [dict valueForKey:key];
	if (value && [value isKindOfClass:[NSString class]]) {
		return [UIColor PA_colorWithHexString:value];
	}
	else {
		
		if (optional) return nil;
		PALogError(@"%@",[NSString stringWithFormat:@"%@ is not a color",key]);
	}
	
	return nil;
}

+ (BOOL) checkIsArray: (id) object key:(NSString *)key optional:(BOOL)optional {
	return [PADataParser checkObjectClass:object class:[NSArray class] key:key optional:optional];
}

+ (BOOL) checkIsDictionary: (id) object key:(NSString *)key optional:(BOOL)optional {
	return [PADataParser checkObjectClass:object class:[NSDictionary class] key:key optional:optional];
}

+ (BOOL) checkIsString: (id) object key:(NSString *)key optional:(BOOL)optional {
	return [PADataParser checkObjectClass:object class:[NSString class] key:key optional:optional];
}

+ (BOOL) checkIsNumber: (id) object key:(NSString *)key optional:(BOOL)optional {
	return [PADataParser checkObjectClass:object class:[NSNumber class] key:key optional:optional];
}

+ (BOOL) checkIsDate: (id) object key:(NSString *)key optional:(BOOL)optional {
	return [PADataParser checkObjectClass:object class:[NSDate class] key:key optional:optional];
}

+ (BOOL) checkObjectClass: (id) object class: (Class) c key:(NSString *)key optional:(BOOL) optional{
	if (object && [object isKindOfClass:c]) return YES;
	PALogError(@"%@",[NSString stringWithFormat:@"%@ is not %@",key,NSStringFromClass(c)]);
	
	return NO;
}

@end
