//
//  PAFramework.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAFramework.h"
#import "PAMainApp.h"
#import "PADefinitions.h"
#import "PALog.h"
#import "PAUtils.h"
#import "PANotifications.h"

#import <AFNetworking/AFNetworking.h>
#import "UIImageView+AFNetworking.h"

#ifdef PAFW_FW_MAIN_APP_AVAILABLE
#import "TNDataManager.h"
#import "TNApp.h"
#endif

#ifdef FUCKING_WEBTRENDS_IS_INCLUDED
#import <Webtrends/Webtrends.h>
#endif

static BOOL debugMode = NO;
static NSMutableArray *favorites;

@implementation PAFramework

+ (void) enableDebugMode {
	debugMode = YES;
}

+ (void) disableDebugMode {
	debugMode = NO;
}


+ (BOOL) inDebugMode {
	return debugMode;
}

+ (float) version {
	return 1.0f;
}

+ (BOOL) mainAppAvailable {
	if (debugMode) {
//		[core_fw_Log Log:@"in debug mode. main app shows as available" kind:@"core" level:core_fw_NOTICE];
		return YES;
	}
	
	return [PAMainApp mainAppAvailable];
}


+ (void) showMainApp {
	
	if ([PAMainApp mainAppAvailable]) {
//		[core_fw_Log Log:@"Go to main app. It is available" kind:@"core" level:core_fw_NOTICE];
		[[NSNotificationCenter defaultCenter] postNotificationName:PAFW_SHOW_MAINAPP_NOTIFICATION object:nil];
	}
	else {
		
		if (debugMode) {
//			[core_fw_Log Log:@"Go to main app debug mode" kind:@"core" level:core_fw_NOTICE];
			
			NSString *title = NSLocalizedString(@"Debug Mode", nil);
			NSString *message = NSLocalizedString(@"Showing main app", nil);
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
			[alertView show];
		}
		else {
//			[core_fw_Log Log:@"Trying to go to main app, but it is not available" kind:@"core" level:core_fw_ERROR];
			NSString *title = NSLocalizedString(@"Error", nil);
			NSString *message = NSLocalizedString(@"Main App is not available", nil);
			
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles: nil];
			[alertView show];
		}
		
	}
}

+ (void) openApp: (NSString *) appId parameters:(NSDictionary *)parameters {
	NSLog(@"open app: %@",appId);
	if (!parameters) parameters = (NSDictionary *)[NSNull null];
	[[NSNotificationCenter defaultCenter] postNotificationName:PAFW_OPEN_APP_NOTIFICATION object:@{ @"id" : appId, @"parameters" : parameters }];
}

+ (BOOL) isInstalled:(NSString *)appId {
#ifdef PAFW_FW_MAIN_APP_AVAILABLE
	
	
	NSArray *allApps = [PAMainApp allApps];
	for (NSString *ident in allApps) {
		if ([ident isEqualToString:appId]) return YES;
	}
	
	return NO;
//	return ([[DataManager sharedInstance] appInfo:appId]!=nil);
	
#else
	return NO;
#endif
}


+ (BOOL) isAccesible:(NSString *)appId {
	// TODO See how to handle this
	return [PAFramework isInstalled:appId];
}

+ (NSString *) currentAppId {
	return [PAMainApp currentAppId];
}

#pragma mark Initialization and finalization methods

+ (void) init {
//	core_fw_Database *db = [core_fw_Database Get];
//	[db initDB];
//	[db loadData];
	
	NSURLCache *URLCache = [[NSURLCache alloc] initWithMemoryCapacity:8 * 1024 * 1024
														 diskCapacity:32 * 1024 * 1024
															 diskPath:@"paframework"];
	[NSURLCache setSharedURLCache:URLCache];
	sleep(0.5);
	
	[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidReceiveMemoryWarningNotification object:nil queue:nil usingBlock:^(NSNotification *note) {
		dispatch_async(dispatch_get_main_queue(), ^{
			[[NSURLCache sharedURLCache] removeAllCachedResponses];
		});
	}];
	
	[[PALog sharedInstance] setup];
}


+ (void) finish {
}

#pragma marks Favorites

+ (void) addFavorite: (PAFavorite *) favorite {

	if (!favorites) [PAFramework loadFavorites];
	
	[favorites addObject:favorite];
	
	[PAFramework saveFavorites];
}

+ (void) removeFavorite: (NSString *) ident appId:(NSString *) appId {
	PAFavorite *favorite = nil;
	for (PAFavorite *fav in [PAFramework favorites]) {
		if ([fav.appId isEqualToString:appId] && [fav.itemId isEqualToString:ident]) {
			favorite = fav;
			break;
		}
	}

	if (favorite) {
		[favorites removeObject:favorite];
		[PAFramework saveFavorites];
	}
}

+ (BOOL) isFavorite:(NSString *) ident appId:(NSString *) appId {
	[self loadFavorites];
	for (PAFavorite *fav in [PAFramework favorites]) {
		if ([fav.appId isEqualToString:appId] && [fav.itemId isEqualToString:ident]) return YES;
	}
	return NO;
}

+ (NSArray *) favorites {
	[self loadFavorites];
	return favorites;
}

+ (PAUser *)currentUser {
	return [PAMainApp currentUser];
}

+ (NSArray *) serializeFavorites {
	NSMutableArray *results = [NSMutableArray arrayWithCapacity:favorites.count];
	for (PAFavorite *fav in favorites) {
		[results addObject:[fav serialize]];
	}
	
	return results;
}

+ (void) loadFavorites: (NSArray *) serialized {
	favorites = [NSMutableArray arrayWithCapacity:serialized.count];
	for (NSDictionary *dict in serialized) {
		PAFavorite *fav = [[PAFavorite alloc] init];
		[fav load:dict];
		[favorites addObject:fav];
	}
}


+ (void) saveFavorites {
	[[NSUserDefaults standardUserDefaults] setValue:[PAFramework serializeFavorites] forKey:@"PAFavorites"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) loadFavorites {
	NSArray *serialized = [[NSUserDefaults standardUserDefaults] valueForKey:@"PAFavorites"];
	if (serialized) [PAFramework loadFavorites:serialized];
	else favorites = [NSMutableArray array];
//	if (favorites) favorites = [favorites mutableCopy];
//	else favorites = [NSMutableArray array];
}

+ (void) loadFile:(NSURL *)url completion:(PADataCompletionBlock)block {
	NSMutableURLRequest	*request = [NSMutableURLRequest requestWithURL:url];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	[op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
		if (block) block(operation.responseData,nil);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		if (block) block(nil,error);
	}];
	
	[op start];
}

+ (void) loadImage:(NSURL *)url completion:(PAImageCompletionBlock)block {
	
	NSURL *originalUrl = url;
	
	NSString *lastComponent = [url lastPathComponent];
	NSString *name = [lastComponent stringByDeletingPathExtension];
	NSString *extension = [lastComponent pathExtension];
	lastComponent = [name stringByAppendingString:@"@2x"];
	lastComponent = [lastComponent stringByAppendingPathExtension:extension];
	url = [[url URLByDeletingLastPathComponent] URLByAppendingPathComponent:lastComponent];
	
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	
	AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
	op.responseSerializer = [AFImageResponseSerializer serializer];
	[op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, UIImage *image) {
		CGFloat scale = 2;
		if ([request.URL.absoluteString rangeOfString:@"@2x"].location==NSNotFound) scale = 1;
		
		UIImage *finalImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
		
		if (block) block(finalImage,nil);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		if ([url.absoluteString isEqualToString:originalUrl.absoluteString]){
			if (block) block(nil,error);
		}
		else {
			NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:originalUrl];
			NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
			[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
				[request setValue:obj forHTTPHeaderField:key];
			}];
			
			AFHTTPRequestOperation *op2 = [[AFHTTPRequestOperation alloc] initWithRequest:request];
			op2.responseSerializer = [AFImageResponseSerializer serializer];
			[op2 setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, UIImage *image) {
				CGFloat scale = 2;
				if ([request.URL.absoluteString rangeOfString:@"@2x"].location==NSNotFound) scale = 1;
				
				UIImage *finalImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
				
				if (block) block(finalImage,nil);

			} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
				if (block) block(nil,error);
			}];
			
//			AFImageRequestOperation *op2 = [AFImageRequestOperation imageRequestOperationWithRequest:request imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//				
//				CGFloat scale = 2;
//				if ([request.URL.absoluteString rangeOfString:@"@2x"].location==NSNotFound) scale = 1;
//				
//				UIImage *finalImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
//				
//				if (block) block(finalImage,nil);
//			} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//				if (block) block(nil,error);
//			}];
			[op2 start];
		}
	}];
	
	
//	AFImageRequestOperation *op = [AFImageRequestOperation imageRequestOperationWithRequest:request imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//		
//		CGFloat scale = 2;
//		if ([request.URL.absoluteString rangeOfString:@"@2x"].location==NSNotFound) scale = 1;
//		
//		UIImage *finalImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
//		
//		if (block) block(finalImage,nil);
//	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//		
//		if ([url.absoluteString isEqualToString:originalUrl.absoluteString]){
//			if (block) block(nil,error);
//		}
//		else {
//			NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:originalUrl];
//			NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
//			[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
//				[request setValue:obj forHTTPHeaderField:key];
//			}];
//			AFImageRequestOperation *op2 = [AFImageRequestOperation imageRequestOperationWithRequest:request imageProcessingBlock:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//				
//				CGFloat scale = 2;
//				if ([request.URL.absoluteString rangeOfString:@"@2x"].location==NSNotFound) scale = 1;
//				
//				UIImage *finalImage = [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
//				
//				if (block) block(finalImage,nil);
//			} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//				if (block) block(nil,error);
//			}];
//			[op2 start];
//		}
//	}];
	
	[op start];
}

+ (void)loadImageView:(UIImageView *)imageView withURL:(NSURL *)url andPlaceholder:(UIImage *)placeholder completion:(PACompletionBlock)block {
	[imageView setImageWithURLRequest:[NSURLRequest requestWithURL:url] placeholderImage:placeholder success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
		if (block) block(nil);
	} failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
		if (block) block(error);
	}];
}

+ (NSString *)langCode {

//	NSString *lang = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
//	if (![lang isEqualToString:@"de"] && ![lang isEqualToString:@"it"] && ![lang isEqualToString:@"fr"]) lang = @"en";
//	return lang;
//	

	NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
	lang = [lang componentsSeparatedByString:@"-"].firstObject;
	if (![lang isEqualToString:@"de"] && ![lang isEqualToString:@"it"] && ![lang isEqualToString:@"fr"]) lang = @"en";
	return lang;
}

+ (void)registerEventWithApp:(NSString *)appName {
#ifdef FUCKING_WEBTRENDS_IS_INCLUDED
	// Call the fucking webtrends methods
	WTEvent *event = [WTEvent eventForAppLaunch:WTLaunchStyleFromHomeScreen applicationName:appName];
	[[self class] trackEvent:event];
#endif
}

@end
