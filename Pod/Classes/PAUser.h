//
//  TNUser.h
//  Extranet
//
//  Created by Javier Querol on 28/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PAUser : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *identifier;
@property (strong, nonatomic) NSString *login;
@property (strong, nonatomic) NSMutableArray *profiles;

@property (strong, nonatomic) NSDictionary *dictionary;

- (id)initWithDic:(NSDictionary *)dic;

@end
