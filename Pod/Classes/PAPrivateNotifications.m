//
//  PAPrivateNotifications.m
//  Extranet
//
//  Created by Javier Querol on 16/05/13.
//  Copyright (c) 2013 treeNovum. All rights reserved.
//

#import "PAPrivateNotifications.h"
#import "PADefinitions.h"
#import "PAUtils.h"

#import "PAURLLoader.h"

#import "PALog.h"

static PAPrivateNotifications *instance = nil;
static NSString *deviceServerId = nil;

@implementation PAPrivateNotifications

// EMPTY IMPLEMENTATION SO THE SUBAPP DEVELOPERS CAN TEST THIS
+ (PAPrivateNotifications *)Get {
	if (!instance) {
		instance = [[PAPrivateNotifications alloc] init];
	}
	
	return instance;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	if (connection==tagSettingsConnection) {
		tagsData = [[NSMutableData alloc] init];
	}
	else if (connection==saveTagSettingsConnection) {
		saveTagsData = [[NSMutableData alloc] init];
	}
	else if (connection==breakIntervalConnection) {
		breakIntervalData = [[NSMutableData alloc] init];
	}
	else if (connection==saveBreakIntervalConnection) {
		saveBreakIntervalData = [[NSMutableData alloc] init];
	}
	else {
		data = [[NSMutableData alloc] init];
	}
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)newData
{
	if (connection==tagSettingsConnection) {
		[tagsData appendData:newData];
	}
	else if (connection==saveTagSettingsConnection) {
		[saveTagsData appendData:newData];
	}
	else if (connection==breakIntervalConnection) {
		[breakIntervalData appendData:newData];
	}
	else if (connection==saveBreakIntervalConnection) {
		[saveBreakIntervalData appendData:newData];
	}
	else {
		[data appendData:newData];
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	
	PALogError(@"Connection error: %@",error);
	
	if (connection==tagSettingsConnection) {
		tagsData = nil;
		tagSettingsConnection = nil;
	}
	else if (connection==saveTagSettingsConnection) {
		saveTagsData = nil;
		saveTagSettingsConnection = nil;
	}
	else if (connection==breakIntervalConnection) {
		breakIntervalData = nil;
		breakIntervalConnection = nil;
	}
	else if (connection==saveBreakIntervalConnection) {
		saveBreakIntervalData = nil;
		saveBreakIntervalConnection = nil;
	}
	else {
		data = nil;
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	if (connection==tagSettingsConnection) {
		NSString *result = [[NSString alloc] initWithData:tagsData encoding:NSUTF8StringEncoding];
		
		@try {
			NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil]];
			
			if ([settings objectForKey:@"result"] && ![[settings valueForKey:@"result"] boolValue]) {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Error reading tags: %@",settings] kind:@"PUSH" level:core_fw_ERROR];
				if (tagSettingsBlock) {
					tagSettingsBlock(nil);
					tagSettingsBlock = nil;
				}
			}
			else {
				if (tagSettingsBlock) {
					tagSettingsBlock(settings);
					tagSettingsBlock = nil;
				}
			}
		}
		@catch(NSException *e) {
			//			[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing connection data. %@",[e reason]] kind:@"PUSH" level:core_fw_ERROR];
		}
		
		tagSettingsConnection = nil;
	}
	else if (connection==saveTagSettingsConnection) {
		NSString *result = [[NSString alloc] initWithData:saveTagsData encoding:NSUTF8StringEncoding];
		
		@try {
			// { "result" : true }
			NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
			if (resultDict && ![[resultDict valueForKey:@"result"] boolValue]) {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Error saving tags: %@",resultDict] kind:@"PUSH" level:core_fw_ERROR];
			}
			
			BOOL success = [[resultDict valueForKey:@"result"] boolValue];
			
			if (saveTagBlock) {
				saveTagBlock(success);
				saveTagBlock = nil;
			}
		}
		@catch(NSException *e) {
			//			[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing connection data. %@",[e reason]] kind:@"PUSH" level:core_fw_ERROR];
		}
		
		
		saveTagSettingsConnection = nil;
	}
	else if (connection==breakIntervalConnection) {
		NSString *result = [[NSString alloc] initWithData:breakIntervalData encoding:NSUTF8StringEncoding];
		
		@try {
			
			NSMutableDictionary *settings = [NSMutableDictionary dictionaryWithDictionary:[NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil]];
			
			if ([settings objectForKey:@"result"] && ![[settings valueForKey:@"result"] boolValue]) {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Error reading breaktime: %@",settings] kind:@"PUSH" level:core_fw_ERROR];
				if (breakIntervalBlock) {
					breakIntervalBlock(nil);
					breakIntervalBlock = nil;
				}
			}
			else {
				if (breakIntervalBlock) {
					breakIntervalBlock(settings);
					breakIntervalBlock = nil;
				}
			}
			
		}
		@catch(NSException *e) {
			//			[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing connection data. %@",[e reason]] kind:@"PUSH" level:core_fw_ERROR];
		}
		
		
		breakIntervalConnection = nil;
	}
	else if (connection==saveBreakIntervalConnection) {
		NSString *result = [[NSString alloc] initWithData:saveBreakIntervalData encoding:NSUTF8StringEncoding];
		//		[core_fw_Log Log:[NSString stringWithFormat:@"Save break interval result: %@",result] kind:@"PUSH" level:core_fw_DEBUG];
		@try {
			// { "result" : true }
			NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
			if (resultDict && ![[resultDict valueForKey:@"result"] boolValue]) {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Error saving breaktime: %@",resultDict] kind:@"PUSH" level:core_fw_ERROR];
			}
			
			BOOL success = [[resultDict valueForKey:@"result"] boolValue];
			if (saveBreakIntervalBlock) {
				saveBreakIntervalBlock(success);
				saveBreakIntervalBlock = nil;
			}
		}
		@catch(NSException *e) {
			//			[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing connection data. %@",[e reason]] kind:@"PUSH" level:core_fw_ERROR];
		}
		
		saveBreakIntervalConnection = nil;
	}
	else {
		deviceServerId = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		
		@try {
			
			//			NSArray *result = [deviceServerId JSONValue];
			//			NSDictionary *dict = [result objectAtIndex:0];
			NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[deviceServerId dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
			//NSDictionary *dict = [result objectAtIndex:0];
			
			NSString *answer = [dict valueForKey:@"userDevice_id"];
			if (answer) {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Device ID: %@",answer] kind:@"PUSH" level:core_fw_DEBUG];
				[[NSUserDefaults standardUserDefaults] setValue:answer forKey:@"core_app_push_notification_id"];
				[[NSUserDefaults standardUserDefaults] synchronize];
			}
			else {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"No server id after registering token server response: %@",deviceServerId] kind:@"ParseError" level:core_fw_ERROR];
			}
		}
		@catch (NSException *exception) {
			//			[core_fw_Log Log:@"Error parsing register token server response" kind:@"ParseError" level:core_fw_ERROR];
		}
		
		
		data = nil;
	}
}

+ (NSString *)getNotificationId {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"core_app_push_notification_id"];
}


+ (void) registerDeviceToken: (NSString *) token {
	
	//	[core_fw_Log Log:@"register push token" kind:@"notifications" level:core_fw_DEBUG];
	
	// TODO Change this
	NSString *uuid = [PAUtils uniqueDeviceIdentifier];
	NSString *params = [NSString stringWithFormat:@"appContainer_id=%@&phone_id=%@&registration_id=%@&os=2",@"pa0001",uuid,token];
	
	NSData *postData = nil;
	NSString *postLength = nil;
	if (params) {
		postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
	}
	
	NSURL *url = [NSURL URLWithString:REGISTER_PUSH_URL];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	if (params) {
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:postData];
		NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:[PAPrivateNotifications Get]];
		[connection start];
	}
}

- (BOOL) tagSettings: (NSString *) appId completion:(NotificationsSettingsBlock)block {
	
	if (tagSettingsConnection) {
		//		[core_fw_Log Log:@"tag settings connection already started" kind:@"notifications" level:core_fw_ERROR];
		return NO;
	}
	
	//	[core_fw_Log Log:@"tag settings read connection" kind:@"notifications" level:core_fw_DEBUG];
	
	NSNumber *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"core_app_push_notification_id"];
	NSLog(@"%@ %@",[deviceId class],[NSNumber class]);
	if (!deviceId || ![deviceId isKindOfClass:[NSNumber class]]) return NO;
	//	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[deviceId stringValue],@"userDevice_id",appId,@"subApp_id", nil];
	//
	//	NSString *jsonString = @"";
	//	@try {
	//		jsonString = [dict JSONRepresentation];
	//		jsonString = [core_fw_Utils escapeURLString:jsonString];
	//	}
	//	@catch (NSException *exception) {
	//		[core_fw_Log Log:[NSString stringWithFormat:@"Error converting parameters into json representation %@",[exception reason]] kind:@"JSON" level:core_fw_ERROR];
	//		return NO;
	//	}
	
	if (block) tagSettingsBlock = block;
	
	NSString *params = [NSString stringWithFormat:@"operation=0&userDevice_id=%@&subApp_id=%@",[deviceId stringValue],appId];
	/*NSData *postData = nil;
	 NSString *postLength = nil;
	 if (params) {
	 postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	 postLength = [NSString stringWithFormat:@"%d", [postData length]];
	 }*/
	
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",TAGS_PUSH_URL,params]];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
	//[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	tagSettingsConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	[tagSettingsConnection start];
	
	return YES;
	
	/*NSString *urlString = [NSString stringWithFormat:@"%@?operation=0&json=%@",TAGS_PUSH_URL,jsonString];
	 NSURL *url = [NSURL URLWithString:urlString];
	 NSStringEncoding encoding = NSUTF8StringEncoding;
	 NSError *error = nil;
	 
	 //NSMutableDictionary *finalDict = [NSMutableDictionary dictionary];
	 NSString *result = [NSString stringWithContentsOfURL:url encoding:encoding error:&error];
	 if (result) {
	 @try {
	 //return [result JSONValue];
	 
	 
	 //			for (NSDictionary *setting in settings) {
	 //				NSNumber *selected = [setting valueForKey:@"selected"];
	 //				NSNumber *enabled = [NSNumber numberWithBool:NO];
	 //				if ([selected intValue]==1) enabled = [NSNumber numberWithBool:YES];
	 //
	 //				if ([[setting valueForKey:@"name"] isEqualToString:@"this"]) {
	 //					[finalDict setValue: enabled forKey:appId];
	 //				}
	 //				else [finalDict setValue: enabled forKey:[setting valueForKey:@"name"]];
	 //			}
	 }
	 @catch (NSException *exception) {
	 [core_fw_Log Log:[NSString stringWithFormat:@"Error parsing parameters from json representation %@",[exception reason]] kind:@"JSON" level:core_fw_ERROR];
	 }
	 }
	 
	 //return finalDict;
	 return nil;*/
	
	/*// TEST
	 NSMutableDictionary *aux = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],@"enabledTag",[NSNumber numberWithBool:NO],@"disabledTag", nil];
	 [aux setValue:[NSNumber numberWithBool:YES] forKey:appId];
	 return aux;*/
}


- (BOOL) saveTagSettings: (NSDictionary *)settings forApp: (NSString *) appId completion:(NotificationsSaveBlock)block {
	
	if (saveTagSettingsConnection) {
		//		[core_fw_Log Log:@"save tag settings connection already started" kind:@"notifications" level:core_fw_ERROR];
		return NO;
	}
	
	//	[core_fw_Log Log:@"tag settings save connection" kind:@"notifications" level:core_fw_DEBUG];
	
	NSNumber *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"core_app_push_notification_id"];
	if (!deviceId || ![deviceId isKindOfClass:[NSNumber class]]) return NO;
	
	//NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[deviceId stringValue],@"userDevice_id",appId,@"subApp_id", nil];
	
	NSString *jsonString = @"";
	@try {
		jsonString = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:settings
																					options:NSJSONWritingPrettyPrinted
																					  error:nil] encoding:NSUTF8StringEncoding];
		jsonString = [PAUtils escapeURLString:jsonString];
	}
	@catch (NSException *exception) {
		//		[core_fw_Log Log:[NSString stringWithFormat:@"Error converting parameters into json representation %@",[exception reason]] kind:@"JSON" level:core_fw_ERROR];
		return NO;
	}
	
	if (block) saveTagBlock = block;
	
	NSString *params = [NSString stringWithFormat:@"operation=1&userDevice_id=%@&subApp_id=%@&tags=%@",[deviceId stringValue],appId,jsonString];
	NSData *postData = nil;
	NSString *postLength = nil;
	if (params) {
		postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
	}
	
	NSURL *url = [NSURL URLWithString:TAGS_PUSH_URL];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	if (params) {
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:postData];
		saveTagSettingsConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
		[saveTagSettingsConnection start];
	}
	
	return YES;
}

- (BOOL) breakIntervalSettings: (NSString *)appId completion:(NotificationsBreakIntervalBlock)block {
	
	if (breakIntervalConnection) {
		//		[core_fw_Log Log:@"break interval settings connection already started" kind:@"notifications" level:core_fw_ERROR];
		return NO;
	}
	
	//	[core_fw_Log Log:@"break interval" kind:@"notifications" level:core_fw_DEBUG];
	
	NSNumber *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"core_app_push_notification_id"];
	if (!deviceId || ![deviceId isKindOfClass:[NSNumber class]]) return NO;
	
	if (block) breakIntervalBlock = block;
	
	NSString *params = [NSString stringWithFormat:@"operation=0&userDevice_id=%@&subApp_id=%@",deviceId,appId];
	/*NSData *postData = nil;
	 NSString *postLength = nil;
	 if (params) {
	 postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	 postLength = [NSString stringWithFormat:@"%d", [postData length]];
	 }*/
	
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",BREAKTIME_PUSH_URL,params]];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
	//[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	breakIntervalConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
	[breakIntervalConnection start];
	
	return YES;
	
	//	if (params) {
	//		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	//		[request setHTTPBody:postData];
	//		NSURLConnection *connection = [[[NSURLConnection alloc] initWithRequest:request delegate:[core_fw_NotificationsManager Get]] autorelease];
	//		[connection start];
	//	}
	
	
	/*// TODO Change this
	 NSString *uuid = [core_fw_Utils uniqueDeviceIdentifier];
	 NSString *params = [NSString stringWithFormat:@"appContainer_id=%@&phone_id=%@&registration_id=%@&os=2",@"pa0001",uuid,token];
	 
	 NSData *postData = nil;
	 NSString *postLength = nil;
	 if (params) {
	 postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
	 postLength = [NSString stringWithFormat:@"%d", [postData length]];
	 }
	 
	 NSURL *url = [NSURL URLWithString:REGISTER_PUSH_URL];
	 
	 NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT] autorelease];
	 [request setHTTPMethod:@"POST"];
	 [request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	 [request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	 [request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	 
	 if (params) {
	 [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
	 [request setHTTPBody:postData];
	 NSURLConnection *connection = [[[NSURLConnection alloc] initWithRequest:request delegate:[core_fw_NotificationsManager Get]] autorelease];
	 [connection start];
	 }
	 
	 */
}


- (BOOL) saveBreakIntervalSettings: (NSDictionary *) settings forApp: (NSString *) appId completion:(NotificationsSaveBlock)block {
	
	if (saveBreakIntervalConnection) {
		//		[core_fw_Log Log:@"save break interval settings connection already started" kind:@"notifications" level:core_fw_ERROR];
		return NO;
	}
	
	//	[core_fw_Log Log:@"breaktime settings save connection" kind:@"notifications" level:core_fw_DEBUG];
	
	if (!settings) return NO;
	
	NSNumber *deviceId = [[NSUserDefaults standardUserDefaults] valueForKey:@"core_app_push_notification_id"];
	if (!deviceId || ![deviceId isKindOfClass:[NSNumber class]]) return NO;
	
	//NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithObjectsAndKeys:[deviceId stringValue],@"userDevice_id",appId,@"subApp_id", nil];
	
	//	NSString *jsonString = @"";
	//	@try {
	//		jsonString = [settings JSONRepresentation];
	//		jsonString = [core_fw_Utils escapeURLString:jsonString];
	//	}
	//	@catch (NSException *exception) {
	//		[core_fw_Log Log:[NSString stringWithFormat:@"Error converting parameters into json representation %@",[exception reason]] kind:@"JSON" level:core_fw_ERROR];
	//		return NO;
	//	}
	
	
	NSString *start = [settings valueForKey:@"start"];
	NSString *stop = [settings valueForKey:@"stop"];
	NSNumber *enabledNumber = [settings valueForKey:@"enabled"];
	
	if (!start || !stop || !enabledNumber) {
		return NO;
	}
	
	NSString *enabled = @"false";
	if ([enabledNumber boolValue]) enabled = @"true";
	
	if (block) saveBreakIntervalBlock = block;
	
	NSString *params = [NSString stringWithFormat:@"operation=1&userDevice_id=%@&subApp_id=%@&start=%@&stop=%@&enabled=%@",[deviceId stringValue],appId,start,stop,enabled];
	NSData *postData = nil;
	NSString *postLength = nil;
	if (params) {
		postData = [params dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
		postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
	}
	
	NSURL *url = [NSURL URLWithString:BREAKTIME_PUSH_URL];
	
	NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:CONNECTION_TIMEOUT];
	[request setHTTPMethod:@"POST"];
	[request setValue:@"application/x-www-form-urlencoded, charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
	[request setValue:@"postauto" forHTTPHeaderField:@"User-Agent"];
	[request setValue:@"en-us" forHTTPHeaderField:@"Accept-Language"];
	
	NSDictionary *infoParams = [PAURLLoader infoHeaderParams];
	[infoParams enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[request setValue:obj forHTTPHeaderField:key];
	}];
	
	if (params) {
		[request setValue:postLength forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:postData];
		saveBreakIntervalConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
		[saveBreakIntervalConnection start];
	}
	
	return YES;
}

- (void) loadWholeMessage: (NSDictionary *)userInfo completion: (NotificationsLoadWholeBlock) block {
	dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSNumber *msgId = [userInfo valueForKey:@"msg_id"];
		if (msgId) {
			NSError *error = nil;
			NSStringEncoding encoding = NSUTF8StringEncoding;
			NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?msg_id=%d",MESSAGE_PUSH_URL,[msgId intValue]]];
			NSString *result = [NSString stringWithContentsOfURL:url encoding:encoding error:&error];
			if (result) {
				@try {
					NSDictionary *resultDict = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:nil];
					if (resultDict) {
						dispatch_async(dispatch_get_main_queue(), ^{
							block(resultDict);
						});
					}
				}
				@catch (NSException *exception) {
					//					[core_fw_Log Log:[NSString stringWithFormat:@"Error parsing the whole message: %@",[exception reason]] kind:@"PUSH" level:core_fw_ERROR];
				}
			}
			else {
				//				[core_fw_Log Log:[NSString stringWithFormat:@"Error retrieving the whole message: %d %@",[error code],[error description]] kind:@"PUSH" level:core_fw_ERROR];
			}
		}
		else {
			//			[core_fw_Log Log:[NSString stringWithFormat:@"Error in the push notification dictionary: %@",userInfo] kind:@"PUSH" level:core_fw_ERROR];
		}
	});
}


@end
