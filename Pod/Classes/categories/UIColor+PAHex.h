//
//  UIColor+Hex.h
//  color
//
//  Created by Andrew Sliwinski on 9/15/12.
//  Copyright (c) 2012 Andrew Sliwinski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PAHex)

+ (UIColor *)PA_colorWithHex:(UInt32)hex andAlpha:(CGFloat)alpha;

+ (UIColor *)PA_colorWithHex:(UInt32)hex;
+ (UIColor *)PA_colorWithHexString:(id)input;
+ (UIColor *)PA_colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha;
+ (UIColor *)PA_colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha;

@end
