Pod::Spec.new do |s|
  s.name             = "PAFramework"
  s.version          = "0.2.1"
  s.summary          = "A short description of PAFramework."
  s.description      = <<-DESC
  PAFramework is the supporting framework of PostAuto and PAINT apps. It can be used by subapps to go back to the main navigation, to check if the subapp is running standalone or integrated into PostAuto, among other uses.
                       DESC
  s.homepage         = "https://bitbucket.org/treenovum_ios/paframework"
  s.license          = 'MIT'
  s.author           = { "José Manuel" => "josema.sanchez@treenovum.es" }
  s.source           = { :git => "https://bitbucket.org/treenovum_ios/paframework.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'PAFramework' => ['Pod/Assets/*.png']
  }

  s.dependency 'CocoaLumberjack', '~> 3.0'
  s.dependency 'AFNetworking', '~> 2.5'
end
