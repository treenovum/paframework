//
//  main.m
//  PAFramework
//
//  Created by José Manuel on 02/26/2015.
//  Copyright (c) 2014 José Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PAAppDelegate class]));
    }
}
