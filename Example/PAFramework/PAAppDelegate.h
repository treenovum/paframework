//
//  PAAppDelegate.h
//  PAFramework
//
//  Created by CocoaPods on 02/26/2015.
//  Copyright (c) 2014 José Manuel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
